/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import type {Node} from 'react';
import {
	StyleSheet,
} from 'react-native';
import Home from 'screens/Home';

const App: () => Node = () => {
	return (
		<Home />
	);
};

export default App;
