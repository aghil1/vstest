import React, {useState} from 'react'
import {
    View,
    FlatList,
    StyleSheet,
    Image,
    TouchableOpacity
} from 'react-native'
import {Colors, Shadow} from 'styles'
import {TextRegular} from 'components/global'
import {ImageAsset} from 'utils'

import TopButton from 'components/section/Home/TopButton'

const Home = ({

}) => {

    const [data, setData] = useState([
        {
            img: ImageAsset.Image1,
            totalLike: 0
        },
        {
            img: ImageAsset.Image2,
            totalLike: 0
        },
        {
            img: ImageAsset.Image3,
            totalLike: 0
        }
    ])

    const increaseAllLike =  () => {
        let arr = [...data];
        arr.map((val, i) => {
            val.totalLike += 1;
        })
        setData(arr);
    }
    
    const decreaseAllLike =  () => {
        let arr = [...data];
        arr.map((val, i) => {
            if(val.totalLike > 0) {
                val.totalLike -= 1;
            }            
        })
        setData(arr);
    }

    const resetAllLike =  () => {
        let arr = [...data];
        arr.map((val, i) => {
            val.totalLike = 0;
        })
        setData(arr);
    }

    const increaseLike =  (index) => {
        let arr = [...data];
        arr.map((val, i) => {
            if(index === i) {
                val.totalLike += 1;
            }
        })
        setData(arr);
    }

    const decreaseLike =  (index) => {
        let arr = [...data];
        arr.map((val, i) => {
            if(index === i) {
                if(val.totalLike > 0) {
                    val.totalLike -= 1;
                }                
            }
        })
        setData(arr);
    }

    return(
        <View style={styles.container}>
            <TopButton
                likeAll={increaseAllLike}
                decreaseAll={decreaseAllLike}
                resetAll={resetAllLike}
            />
            <FlatList
                data={data}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{paddingBottom: 20}}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => (
                    <View style={[styles.cardItem, Shadow.Bottom]}>
                        <Image
                            source={item.img}
                            style={styles.img}
                        />
                        <View style={styles.viewButton}>
                            <View 
                                style={styles.viewTxt}>
                                <TextRegular
                                    text={`${item.totalLike} Like`}                                    
                                />
                            </View>
                            <View style={styles.viewRowBtn}>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={[styles.viewTxt, {backgroundColor: Colors.BLUE}]}
                                    onPress={() => increaseLike(index)}
                                >
                                    <TextRegular
                                        text="Like"
                                        color='#fff'
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity
                                    activeOpacity={0.8}
                                    style={[styles.viewTxt, {backgroundColor: Colors.RED, marginLeft: 8}]}
                                    onPress={() => decreaseLike(index)}
                                >
                                    <TextRegular
                                        text="Dislike"
                                        color='#fff'
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                )}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    viewRowBtn: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    viewTxt: {
        flexDirection: 'row',
        alignItems: 'center',
        borderRadius: 6,
        borderColor: Colors.BORDER_COLOR,
        borderWidth: 1,
        paddingVertical: 5,
        paddingHorizontal: 10
    },
    viewButton: {
        width: '90%',
        alignSelf: 'center',
        marginTop: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between'
    },
    img: {
        width: '100%',
        height: 200,
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        resizeMode: 'cover'
    },
    cardItem: {
        width: '100%',
        borderRadius: 8,
        paddingBottom: 10,
        backgroundColor: '#fff',
        marginTop: 20
    },
    container: {
        flex: 1, 
        backgroundColor: Colors.BG_GREY,
        padding: 10
    }
})

export default Home;