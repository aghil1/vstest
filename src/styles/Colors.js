export const WHITE = '#FFFFFF';
export const RED = '#DB2C2C';
export const BLUE = '#2B72C4';
export const GREY = '#5F5F5F';
export const BG_GREY = '#F4F4F4';
export const BORDER_COLOR = '#00000029';