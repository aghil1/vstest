export const Bottom = {
	shadowColor: 'rgba(46, 50, 132, 0.15)',
	shadowOffset: {
		width: 0,
		height: 12,
	},
	shadowOpacity: 0.58,
	shadowRadius: 16.0,
	elevation: 10,
};