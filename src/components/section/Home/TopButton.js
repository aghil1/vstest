import React from 'react'
import {
    View,
    TouchableOpacity,
    StyleSheet
} from 'react-native'
import {Colors} from 'styles'
import {TextRegular} from 'components/global'

class TopButton extends React.PureComponent {
    render() {
        console.log('cek props', this.props);
        const {likeAll, decreaseAll, resetAll} = this.props;
        return(
            <View style={styles.container}>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={styles.btn}
                    onPress={() => likeAll()}
                >
                    <TextRegular
                        text="Like All"
                        color='#fff'
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={[styles.btn, {backgroundColor: '#fff'}]}
                    onPress={() => resetAll()}
                >
                    <TextRegular
                        text="Reset All"
                        color={Colors.GREY}
                    />
                </TouchableOpacity>
                <TouchableOpacity
                    activeOpacity={0.8}
                    style={[styles.btn, {backgroundColor: Colors.RED}]}
                    onPress={() => decreaseAll()}
                >
                    <TextRegular
                        text="Dislike All"
                        color="#fff"
                    />
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    btn: {
        paddingVertical: 9,
        paddingHorizontal: 25,
        borderRadius: 10,
        flexDirection: 'row',
        alignItems: 'center',
        backgroundColor: Colors.BLUE
    },
    container: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingTop: 10
    }
})

export default TopButton;