import React from 'react';
import {Text, StyleSheet} from 'react-native';
import {Colors} from 'styles'

export const TextRegular = ({
    text, 
    numberOfLines, 
    size = 14, 
    color = Colors.GREY, 
    style
}) => {
	return (
		<Text
			style={[
				styles.regularText,
				{
					fontSize: size, 
					color: color,
				},
				style,
			]}			
			numberOfLines={numberOfLines}
		>
			{text}
		</Text>
	);
};

const styles = StyleSheet.create({
    regularText: {
        fontFamily: 'HelveticaNeue-Regular'
    }
})